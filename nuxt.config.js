module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'mejorseguro',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Template to pages from mejorseguro' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    link: [
          { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Montserrat:400,700,800' }
        ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3bd600' },
  /*
  ** Build configuration
  */
  build: {

    vendor: ['jquery', 'bootstrap'],
    /*
   ** Run ESLint on save
   */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  // include bootstrap css
  css: ['bootstrap/dist/css/bootstrap.css','static/css/ivandesing.css'],
  // include bootstrap js on startup
  //plugins: [{ src: '~/plugins/filters.js', ssr: false }],
  plugins: ['~plugins/bootstrap.js',{ src: '~/plugins/filters.js', ssr: false }],
  modules: [
      ['@nuxtjs/font-awesome'],
      ['@nuxtjs/google-tag-manager', { id: 'GTM-57HSZD2' }],
      ['nuxt-validate', {lang: 'es'}]
  ],
  env: {
    baseUrl: 'https://ahorraseguros.mx/ws-autos/servicios'
  },
  render: {
    http2:{ push: true },
    resourceHints:false,
    gzip:{ threshold: 9 }
  }
}
