import Vuex from 'vuex'
import database from '~/plugins/saveData'
import monitoreo from '~/plugins/monitoreo'
import cotizacion from '~/plugins/cotizacion'

const createStore = () => {
    return new Vuex.Store({
        state: {
            config: {
                aseguradora: '',
                cotizacion: false,
                emision: false,
                descuento: 0,
                telefonoAS: '',
                grupoCallback: '',
                from:'',
                idPagina:0,
                idCampana:0,
                loading:false
            },
            ejecutivo:{
                nombre:'',
                correo:'',
                id:0
            },
            formData: {
                aseguradora:'',
                marca: '',
                modelo: '',
                descripcion: '',
                subDescripcion: '',
                detalle: '',
                detalleid: '',
                codigoPostal: '',
                nombre: '',
                apellidoPaterno: '',
                apellidoMaterno: '',
                telefono: '',
                correo: '',
                edad: '',
                fechaNacimiento:'',
                genero: '',
                precio:''
            },
            solicitud:{},
            cotizacion:{},
            servicios:{
                servicioDB:'http://138.197.128.236:8081/ws-autos/servicios'
            }
        },
        mutations: {
            saveData: function (state) {
                database.search(
                    state.formData.modelo,
                    state.formData.apellidoMaterno,
                    state.formData.apellidoPaterno,
                    state.formData.correo,
                    state.formData.codigoPostal,
                    state.formData.fechaNacimiento,
                    state.config.grupoCallback,
                    state.config.idCampana,
                    state.config.idPagina,
                    state.formData.marca,
                    state.formData.nombre,
                    state.formData.genero,
                    state.formData.descripcion,
                    state.formData.subDescripcion,
                    state.formData.detalle,
                    state.formData.telefono,
                    state.config.telefonoAS,
                    state.config.from,
                    '')
                    .then(resp => {
                        state.solicitud = resp;
                        state.ejecutivo.nombre = resp.nombreEjecutivo;
                        state.ejecutivo.correo = resp.mailEjecutivo;
                        state.ejecutivo.id = resp.idEjecutivo;
                        this.commit('saveMonitoreo');
                    });
            },
            saveMonitoreo: function (state) {
                monitoreo.search(
                    state.formData.modelo,
                    state.config.aseguradora,
                    (state.cotizacion.length>0? 'SI' : 'NO'),
                    state.formData.descripcion,
                    state.formData.detalle,
                    state.config.idCampana,
                    state.ejecutivo.id,
                    state.config.idPagina,
                    state.solicitud.idSolicitud,
                    state.formData.marca,
                    "COTIZACION",
                    state.formData.subDescripcion,
                    '')
                    .then(resp => {
                        //console.log(resp.result)
                    });
            },
            cotizar: function (state) {
                cotizacion.search(
                    state.config.aseguradora,
                    state.formData.detalleid,
                    state.formData.codigoPostal,
                    state.formData.detalle,
                    state.config.descuento,
                    state.formData.edad,
                    ("01/01/" + (new Date().getFullYear() - state.formData.edad).toString()).toString("yyyy/MM/dd"),
                    (state.formData.genero === 'M' ? 'MASCULINO' : 'FEMENINO'),
                    state.formData.marca,
                    state.formData.modelo,
                    'cotizacion',
                    'AMPLIA',
                    'PARTICULAR'
                ).then(resp => {
                    state.cotizacion = resp;
                    if(state.cotizacion.Cotizacion.PrimaTotal){
                        state.formData.precio=state.cotizacion.Cotizacion.PrimaTotal;
                    }

                    //this.limpiarCoberturas();
                    //this.saveData();
                    this.commit('saveData');
                    state.config.loading = false;
                });

            },
            limpiarCoberturas(state) {
                if (state.cotizacion.Coberturas) {
                    for (var c in state.cotizacion.Coberturas[0]) {
                        if (state.cotizacion.Coberturas[0][c] === "-") {
                            delete state.cotizacion.Coberturas[0][c];
                        }
                    }
                }
            }
        }
    })
}

export default createStore