const conversion ={}

conversion.gtag_report_conversion=function(url) {
    var callback = function () {
        if (typeof(url) != 'undefined') {
            window.location = url;
        }
    };
    gtag('event', 'conversion', {
    'send_to': 'AW-800697239/KP6OCNbUioYBEJfX5v0C',
        'event_callback': callback
    });
    return false;
}
export default conversion