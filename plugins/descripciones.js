import autosService from './ws-autos'

const descripcionesService ={}

descripcionesService.search=function (aseguradora, marca, modelo) {
  return autosService.get('/descripciones',{
    params:{aseguradora,marca,modelo}
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default descripcionesService
